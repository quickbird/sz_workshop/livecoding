//  
//  TranslatorViewModel.swift
//  lead
//
//  Created by Stefan Kofler on 10.04.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import Foundation
import RxSwift

protocol TranslatorViewModel {
    var inputEnglishText: BehaviorSubject<String> { get }
    var inputSaveTrigger: PublishSubject<Void> { get }

    var outputGermanText: Observable<String> { get }
    var outputIsSavingAllowed: Observable<Bool> { get }
    var outputSavedGermanTranslation: Observable<String> { get }
}
